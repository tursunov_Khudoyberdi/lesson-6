let backgroundDiv = document.getElementById("backgroundDiv");
let firstBtn = document.getElementById("firstBtn");
let secondBtn = document.getElementById("secondBtn");
let thirdBtn = document.getElementById("thirdBtn");
let fourthBtn = document.getElementById("fourthBtn");
let fifthBtn = document.getElementById("fifthBtn");
let sixthBtn = document.getElementById("sixthBtn");

firstBtn.addEventListener("click", function () {
  backgroundDiv.style.backgroundColor = "rgb(0, 153, 255)";
});

secondBtn.addEventListener("click", function () {
  backgroundDiv.style.backgroundColor = "yellow";
});

thirdBtn.addEventListener("click", function () {
  backgroundDiv.style.backgroundColor = "red";
});

fourthBtn.addEventListener("click", function () {
  backgroundDiv.style.backgroundColor = "blue";
});

fifthBtn.addEventListener("click", function () {
  backgroundDiv.style.backgroundColor = "green";
});

sixthBtn.addEventListener("click", function () {
  backgroundDiv.style.backgroundColor = "rgb(195, 15, 232)";
});
